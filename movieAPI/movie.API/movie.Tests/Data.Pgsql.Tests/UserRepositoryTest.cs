﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using movie.Data.Pgsql.Migrations;
using movie.Data.Pgsql.Repositories;
using movie.IData.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace movie.Tests.Data.Pgsql.Tests
{
   public class UserRepositoryTest
    {
        public IConfiguration Configuration { get; }
        private MovieDbContext _context;
        private IUserRepository _userRepository;

        public UserRepositoryTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<MovieDbContext>();
            optionsBuilder.UseNpgsql(
                "server=localhost;Username=postgres;Password=password;Port=5432;Database=testdb;");
            _context = new MovieDbContext(optionsBuilder.Options);
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
            _userRepository = new UserRepository(_context);
        }

        [Fact]
        public async Task AddUser_Returns_Correct_Response()
        {
            var user = new movie.Domain.User.User("Name", "Surname", "Country", DateTime.UtcNow, "Nickname", "Email");
            var idUser = await _userRepository.AddUser(user);

            var createdUser = await _context.User.FirstOrDefaultAsync(x =>x.idUser == idUser);
            Assert.NotNull(createdUser);

            _context.User.Remove(createdUser);
            await _context.SaveChangesAsync();
        }

    }
}
