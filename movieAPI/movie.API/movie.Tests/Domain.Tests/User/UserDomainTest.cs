﻿using movie.Domain.DomainExceptions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace movie.Tests.Domain.Tests.User
{
    public class UserDomainTest
    {

        [Fact]

        public void CreateUser_Returns_throws_InvalidBirthDateException()
        {
            Assert.Throws<InvalidBirthDateException>
                (() => new movie.Domain.User.User("name", "surname", "country", DateTime.UtcNow.AddHours(1), "nickname", "email"));
        }

        [Fact]
        public void CreateUser_Returns_Correct_Response()
        {
            var user = new movie.Domain.User.User("name", "surname", "country", DateTime.UtcNow, "nickname", "email");

            Assert.Equal("name", user.Name);
            Assert.Equal("surname", user.Surname);
            Assert.Equal("country", user.Country);
            Assert.Equal(DateTime.UtcNow, user.BirthDate);
            Assert.Equal("nickname", user.Nickname);
            Assert.Equal("email", user.Email);
        }
    }
}
