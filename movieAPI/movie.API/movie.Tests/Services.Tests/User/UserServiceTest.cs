﻿
using movie.IData.User;
using movie.IServices.User;
using Moq;
using Xunit;
using movie.Services.User;
using movie.IServices.Requests;
using System;
using movie.Domain.DomainExceptions;
using System.Threading.Tasks;

namespace movie.Tests.Services.Tests.User
{
    public class UserServiceTest
    {
        private readonly IUserService _userService;
        private readonly Mock<IUserRepository> _userRepositoryMock;

        public UserServiceTest()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _userService = new UserService(_userRepositoryMock.Object);
        }

        [Fact]
        public void CreateUser_Returnds_throws_InvalidBirthDateException()
        {
            var user = new CreateUser
            {
                Name = "dupa",
                Country = "polska",
                Email = "email@email.email",
                Surname = "Kowalski",
                BirthDate = DateTime.UtcNow.AddHours(1),
                Nickname = "Dupski"
            };
            Assert.ThrowsAsync<InvalidBirthDateException>(() => _userService.CreateUser(user));
        }


        [Fact]
        public async Task CreateUser_Returns_Correct_Response()
        {
            var user = new CreateUser
            {
                Name = "dupa",
                Country = "polska",
                Email = "email@email.email",
                Surname = "Kowalski",
                BirthDate = DateTime.UtcNow,
                Nickname = "Dupski"
            };

            int expectedResult = 0;
            _userRepositoryMock.Setup(x => x.AddUser(
                new movie.Domain.User.User(
                    user.Name,
                    user.Surname,
                    user.Country,
                    user.BirthDate,
                    user.Nickname,
                    user.Email))).
                    Returns(Task.FromResult(expectedResult));

            var result = await _userService.CreateUser(user);

            Assert.IsType<movie.Domain.User.User>(result);
            Assert.NotNull(result);
            Assert.Equal(expectedResult, result.idUser);
        }
    }
}
