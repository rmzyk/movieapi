﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Domain.Review
{
    public class Review
    {
        public int idReview { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public int ThumbUp { get; set; }
        public int ThumbDown { get; set; }
        public string Text { get; set; }
        
        public Review(int thumbUp, int thumbDown, string text)
        {
            ThumbUp = thumbUp;
            ThumbDown = thumbDown;
            Text = text;
        }
    }


}
