﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Domain.Payment
{
   public class Payment
    {
        public int idPayment { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string PaymentType { get; set; }
        public DateTime PaymentDate { get; set; }

        public Payment(string paymentType, DateTime paymentDate)
        {
            PaymentType = paymentType;
            PaymentDate = paymentDate;
        }
    }
}
