﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Domain.Grade
{
    public class Grade
    {
        public int idGrade { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string Comment { get; set; }
        public float Rating { get; set; }

        public Grade(string comment, float rating)
        {
            Comment = comment;
            Rating = rating;
        }
    }
}
