﻿using System;

namespace movie.Domain.DomainExceptions
{
    class InvalidEmailException : Exception
    {
      public InvalidEmailException(string email): base(ModifyMessage(email)) { }

      private static string ModifyMessage(string email)
        {
            return $"Invalid email address {email}.";
        }
    }
}
