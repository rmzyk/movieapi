﻿using System;

namespace movie.Domain.User
{
    public class User
    {
        public int idUser { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegisterTime { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }


        public User(string name, string surname, string country, DateTime birthDate, string nickname, string email)
        {
            if (BirthDate >= DateTime.UtcNow)
            //    throw new InvalidBirthDateException(birthDate);
            //if (Nickname.IsInvalid())
            //    throw new InvalidNicknameException(nickname);
            //if (Name.IsInvalid())
            //    throw new InvalidNameException(name);
            //if (Email.IsInvalid())
            //    throw new InvalidEmailException(email);

            Name = name;
            Surname = surname;
            Country = country;
            BirthDate = birthDate;
            RegisterTime = DateTime.UtcNow;
            Nickname = nickname;
            Email = email;
        }
    }
}
