﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Domain.Subscription
{
    public class Subscription
    {
        public int idSubscription { get; set; }
        public DateTime DateActivated { get; set; }
        public DateTime ExpireTime { get; set; }
        public string Type { get; set; }
        public string SubscriptionLevel { get; set; }


        public Subscription(DateTime dateActivated, DateTime expireTime, string type, string subscriptionLevel)
        {
            if (expireTime >= DateTime.UtcNow)
                //    throw new InvalidBirthDateException(birthDate);
                //if (Nickname.IsInvalid())
                //    throw new InvalidNicknameException(nickname);
                //if (Name.IsInvalid())
                //    throw new InvalidNameException(name);
                //if (Email.IsInvalid())
                //    throw new InvalidEmailException(email);
                DateActivated = dateActivated;
            ExpireTime = expireTime;
            Type = type;
            SubscriptionLevel = subscriptionLevel;

        }
    }
}
