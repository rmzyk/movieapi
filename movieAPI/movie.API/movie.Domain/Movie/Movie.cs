﻿using System;

namespace movie.Domain.Movie
{
    public class Movie
    {
        public int idMovie { get; set; }
        public string Title { get; set; }
        public DateTime PublicityDate { get; set; }
        public string AgeRestriction { get; set; }
        public float PayPerView { get; set; }
        public string Director { get; set; }
        public int CountPrizes { get; set; }

        public Movie(string title, DateTime publicityDate, string ageRestriction, float payPerView, string director, int countPrizes)
        {
            Title = title;
            PublicityDate = publicityDate;
            AgeRestriction = ageRestriction;
            PayPerView = payPerView;
            Director = director;
            CountPrizes = countPrizes;
        }
    }
}
