﻿using Microsoft.EntityFrameworkCore;
using movie.Data.Pgsql.DAO;
using movie.Data.Pgsql.DAOConfiguration;

namespace movie.Data.Pgsql.Migrations
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }

        public virtual DbSet<Grade> Grade { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<Review> Review { get; set; }
        public virtual DbSet<Subscription> Subscription { get; set; }
        public virtual DbSet<User> User { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new GradeConfiguration());
            builder.ApplyConfiguration(new MovieConfiguration());
            builder.ApplyConfiguration(new PaymentConfiguration());
            builder.ApplyConfiguration(new ReviewConfiguration());
            builder.ApplyConfiguration(new SubscriptionConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
