﻿using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class Grade
    {
        [Key]
        public int idGrade { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string Comment { get; set; }
        public float Rating { get; set; }

        public virtual User User { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
