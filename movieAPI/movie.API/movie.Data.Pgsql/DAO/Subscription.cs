﻿using System;
using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class Subscription
    {
        [Key]
        public int idSubscription { get; set; }
        public DateTime DateActivated { get; set; }
        public DateTime ExpireTime { get; set; }
        public string Type { get; set; }
        public string SubscriptionLevel { get; set; }


        public virtual User User { get; set; }
    }
}
