﻿using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class Review
    {
        [Key]
        public int idReview { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public int ThumbUp { get; set; }
        public int ThumbDown { get; set; }
        public string Text { get; set; }


        public virtual User User { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
