﻿using System;
using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class Payment
    {
        [Key]
        public int idPayment { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string PaymentType { get; set; }
        public DateTime PaymentDate { get; set; }
   


        public virtual Movie Movie { get; set; }
        public virtual User User { get; set; }
    }
}
