﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class User
    {
        public User()
        {
            Payments = new List<Payment>();
            Grades = new List<Grade>();
            Reviews = new List<Review>();
            Subscriptions = new List<Subscription>();
        }
        [Key]
        public int idUser { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegisterTime { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
