﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace movie.Data.Pgsql.DAO
{
    public class Movie
    {
        public Movie()
        {
            Grades = new List<Grade>();
            Payments = new List<Payment>();
            Reviews = new List<Review>();
        }
        [Key]
        public int idMovie { get; set; }
        public string Title { get; set; }
        public DateTime PublicityDate { get; set; }
        public string AgeRestriction { get; set; }
        public float PayPerView { get; set; }
        public string Director { get; set; }
        public int CountPrizes { get; set; }

        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
