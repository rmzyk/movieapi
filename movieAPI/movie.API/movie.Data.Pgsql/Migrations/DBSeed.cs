﻿using movie.Common.Extensions;
using movie.Data.Pgsql.DAO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace movie.Data.Pgsql.Migrations
{
    public class DbSeed
    {
        
        private readonly MovieDbContext _context;

        public DbSeed(MovieDbContext context)
        {
            _context = context;
        }


        #region Methods
        public void Seed()
        {
            var userList = myBuildUserList();
            _context.User.AddRange(userList);
            _context.SaveChanges();

            var subscriptionList = myBuildSubscriptionList(userList);
            _context.Subscription.AddRange(subscriptionList);
            _context.SaveChanges();

            var movieList = myBuildMovieList();
            _context.Movie.AddRange(movieList);
            _context.SaveChanges();

            //var reviewList = myBuildReviewList(userList, movieList);
            //_context.Review.AddRange(reviewList);
            //_context.SaveChanges();

            //var paymentList = myBuildPaymentList(userList, movieList);
            //_context.Payment.AddRange(paymentList);
            //_context.SaveChanges();

            //var gradeList = myBuildGradeList(userList, movieList);
            //_context.Grade.AddRange(gradeList);
            //_context.SaveChanges();
        }
        #endregion Methods


        #region  MyMethods
        private IEnumerable<User> myBuildUserList()
        {
            var userList = new List<User>();

            for(int i =1; i< 100; i++)
                {
                var user = new User()
                {
                idUser = 1 + i,
                Name = "testuser" + i,
                Surname = "xxx" + i,
                Country = "poland" + i,
                BirthDate = new DateTime(2002, 11, 13),
                RegisterTime = new DateTime(2002, 11, 13),
                Nickname = "Jacek" + i,
                Email = "jacek@wacek.klocek" + i
            };
            userList.Add(user);
        }
            userList.Shuffle();
            return userList;
        }

        private IEnumerable<Subscription> myBuildSubscriptionList(
                IEnumerable<User> userList) 
        {
            var subscriptionList = new List<Subscription>();
            string type;
            string level;
            var i = 0;
            foreach(var user in userList)
            {
                i++;
                type = i % 2 == 0 ? "movies" : "series";
                level = i % 2 == 1 ? "1" : "2";
                
                subscriptionList.Add(new Subscription
                {
                    idSubscription = i + 1,
                    DateActivated = DateTime.UtcNow,
                    ExpireTime = DateTime.UtcNow,
                    Type = type,
                    SubscriptionLevel = level,
                    User = user,
                });
            };
            subscriptionList.Shuffle();
            return subscriptionList;
        }

        private IEnumerable<Movie> myBuildMovieList()
        {
            var movieList = new List<Movie>();
            for(int i=0; i<100; i++)
            {
                movieList.Add(new Movie
                {
                    idMovie = i + 1,
                    Title = "BreakingBad" + i,
                    PublicityDate = DateTime.UtcNow,
                    AgeRestriction = "16+",
                    PayPerView = 9.99f,
                    Director = "Vince Galligan" + i,
                    CountPrizes = 110
                });
            }
            movieList.Shuffle();
            return movieList;
        }

        private IEnumerable<Review> myBuildReviewList(
                IEnumerable<User> userList,
                IEnumerable<Movie> movieList)
        {
            var reviewList = new List<Review>();
            var i = 0;
            var usersList = userList.ToList().Count;
            var random = new Random();
            foreach(var movie in movieList)
            {
                var idUser = random.Next(usersList);
            
                reviewList.Add(new Review
                {
                    idReview = 1 + i,
                    idMovie = movie.idMovie,
                    idUser = userList.ToList()[idUser].idUser,
                ThumbUp = 1+i,
                ThumbDown= 1+i,
                Text = "Lorem ipsum lorem ipsum" + i
            }
                );
                i++;
            }
            reviewList.Shuffle();
            return reviewList;
        }

        private IEnumerable<Payment> myBuildPaymentList(
                IEnumerable<User> userList,
                IEnumerable<Movie> movieList)
        {
            var paymentList = new List<Payment>();
            for(int i = 0; i < 99; i++)
            {
                if( i % 2 == 0)
                {
                    paymentList.Add(new Payment
                    {
                        idPayment = 1 + i,
                        PaymentDate = DateTime.UtcNow,
                        PaymentType = "CreditCard",
                    });
                }
            }
            paymentList.Shuffle();
            return paymentList;
        }

        private IEnumerable<Grade> myBuildGradeList(
                IEnumerable<User> userList,
                IEnumerable<Movie> movieList)
        {
            var gradeList = new List<Grade>();
            for(int i = 0; i < 99; i++)
            {
                gradeList.Add(new Grade
                {
                    idGrade = 1 + i,
                    Comment = "Dobry film" + i,
                    Rating = 2,
                });
            }
            gradeList.Shuffle();
            return gradeList;
        }

    }
    #endregion MyMethods
}
