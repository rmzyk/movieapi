﻿using movie.Data.Pgsql.Migrations;
using movie.IData.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace movie.Data.Pgsql.Repositories
{
    public class UserRepository: IUserRepository
    {
        private readonly MovieDbContext _context;

        public UserRepository(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<int> AddUser(Domain.User.User user)
        {
            var userDAO = new DAO.User
            {
                BirthDate = user.BirthDate,
                Country = user.Country,
                Email = user.Email,
                Name = user.Name,
                RegisterTime = user.RegisterTime,
                Nickname = user.Nickname,
                Surname = user.Surname
            };
            await _context.AddAsync(userDAO);
            await _context.SaveChangesAsync();
            return userDAO.idUser;
        }
    }
}
