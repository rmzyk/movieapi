﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(c => c.Email).IsRequired();
            builder.Property(c => c.idUser).IsRequired();
            builder.Property(c => c.RegisterTime).IsRequired();
            builder.Property(c => c.Nickname).IsRequired();
            builder.Property(c => c.BirthDate).IsRequired();
            builder.ToTable("User");
        }
    }
}
