﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.Property(c => c.idMovie).IsRequired();
            builder.Property(c => c.Title).IsRequired();
            builder.Property(c => c.AgeRestriction).IsRequired();
            builder.ToTable("Movie");
        }
    }
}
