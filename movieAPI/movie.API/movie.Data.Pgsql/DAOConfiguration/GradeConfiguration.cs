﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class GradeConfiguration : IEntityTypeConfiguration<Grade>
    {
        public void Configure(EntityTypeBuilder<Grade> builder)
        {
            builder.Property(c => c.Rating).IsRequired();
            builder.Property(c => c.idGrade).IsRequired();

            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Grades)
                .HasForeignKey(x => x.idMovie)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.User)
                .WithMany(x => x.Grades)
                .HasForeignKey(x => x.idUser)
                .OnDelete(DeleteBehavior.Restrict);
                
            builder.ToTable("Grade");
        }
    }
}
