﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class SubscriptionConfiguration : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.Property(c => c.idSubscription).IsRequired();
            builder.Property(c => c.SubscriptionLevel).IsRequired();
            builder.Property(c => c.ExpireTime).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Subscriptions);

            builder.ToTable("Subscription");
        }
    }
}