﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.Property(c => c.idPayment).IsRequired();
            builder.Property(c => c.PaymentDate).IsRequired();

            builder.HasOne(x => x.User).
                WithMany(x => x.Payments)
                 .HasForeignKey(x => x.idUser)
                .OnDelete(DeleteBehavior.Restrict);                
            builder.HasOne(x => x.Movie).
                WithMany(x => x.Payments)
                .HasForeignKey(x => x.idMovie)
                .OnDelete(DeleteBehavior.Restrict);
            builder.ToTable("Payment");
        }
    }
}
