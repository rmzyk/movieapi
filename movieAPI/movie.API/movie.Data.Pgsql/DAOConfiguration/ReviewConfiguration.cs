﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using movie.Data.Pgsql.DAO;

namespace movie.Data.Pgsql.DAOConfiguration
{
    public class ReviewConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.Property(c => c.idReview).IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(x => x.Reviews)
                .HasForeignKey(x => x.idReview)
                .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(x => x.Movie)
                .WithMany(x => x.Reviews)
                .HasForeignKey(x => x.idReview)
                .OnDelete(DeleteBehavior.Restrict);
            builder.ToTable("Review");
        }
    }
}
