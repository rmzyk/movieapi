﻿using System.Threading.Tasks;

namespace movie.IData.Payment
{
    public interface IPaymentRepository
    {
        Task<int> ProcessPayment(Domain.Payment.Payment payment);
    }
}
