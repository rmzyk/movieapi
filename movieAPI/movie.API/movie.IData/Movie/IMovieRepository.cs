﻿using System.Threading.Tasks;

namespace movie.IData.Movie
{
    public interface IMovieRepository
    {
        Task<int> AddMovie(Domain.Movie.Movie movie);
    }
}
