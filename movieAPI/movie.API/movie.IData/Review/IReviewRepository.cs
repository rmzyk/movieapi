﻿using System.Threading.Tasks;

namespace movie.IData.Review
{
    public interface IReviewRepository
    {
        Task<int> AddReview(Domain.Review.Review review);
    }
}
