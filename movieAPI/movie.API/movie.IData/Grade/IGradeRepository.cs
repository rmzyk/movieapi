﻿using System.Threading.Tasks;

namespace movie.IData.Grade
{
    public interface IGradeRepository
    {
        Task<int> Grade(Domain.Grade.Grade grade);
    }
}
