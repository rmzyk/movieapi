﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace movie.IData.Subscription
{
    public interface ISubscriptionRepository
    {
        Task<int> BuySubscription(Domain.Subscription.Subscription subscription);
    }
}
