﻿using System.Threading.Tasks;

namespace movie.IData.User
{
    public interface IUserRepository
    {
        Task<int> AddUser(Domain.User.User user);
    }
}
