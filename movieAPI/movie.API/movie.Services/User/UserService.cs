﻿using movie.IData.User;
using movie.IServices.Requests;
using movie.IServices.User;
using System.Threading.Tasks;

namespace movie.Services.User
{
    public class UserService: IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Domain.User.User> CreateUser(CreateUser createUser)
        {
            var user = new Domain.User.User(createUser.Name, createUser.Surname, createUser.Country, createUser.BirthDate, createUser.Nickname, createUser.Email);
            user.idUser = await _userRepository.AddUser(user);
            return user;
        }
    }
}
