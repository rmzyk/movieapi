﻿using FluentValidation;
using movie.IServices.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Validations
{
    public class CreateUserValidator: AbstractValidator<CreateUser>
    {
        DateTime dateLimit = new DateTime(1940, 1, 1);
        public CreateUserValidator()
        {
            RuleFor(x => x.Nickname).NotNull().MaximumLength(40);
            RuleFor(x => x.Name).NotNull().MaximumLength(40);
            RuleFor(x => x.Surname).NotNull().MaximumLength(40);
            RuleFor(x => x.Email).NotNull().EmailAddress().MaximumLength(40);
            RuleFor(x => x.BirthDate).NotNull().ExclusiveBetween(dateLimit, DateTime.UtcNow);
            RuleFor(x => x.Country).NotNull();
        }
    }
}
