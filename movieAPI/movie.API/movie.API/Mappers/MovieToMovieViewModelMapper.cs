﻿using movie.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Mappers
{
    public class MovieToMovieViewModelMapper
    {
        public static MovieViewModel MovieToMovieViewModel(Domain.Movie.Movie movie)
        {
            var movieViewModel = new MovieViewModel
            {
                idMovie = movie.idMovie,
                AgeRestriction = movie.AgeRestriction,
                CountPrizes = movie.CountPrizes,
                Director = movie.Director,
                PayPerView = movie.PayPerView,
                PublicityDate = movie.PublicityDate,
                Title = movie.Title
            };
            return movieViewModel;
        }
    }
}
