﻿using movie.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Mappers
{
    public class PaymentToPaymentViewModelMapper
    {
        public static PaymentViewModel PaymentToPaymentViewModel(Domain.Payment.Payment payment)
        {
            var paymentViewModel = new PaymentViewModel
            {
                idPayment = payment.idPayment,
                idMovie = payment.idMovie,
                idUser = payment.idUser,
                PaymentDate = payment.PaymentDate,
                PaymentType = payment.PaymentType
            };
            return paymentViewModel;
        }
    }
}
