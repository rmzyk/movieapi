﻿using movie.API.ViewModels;


namespace movie.API.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(Domain.User.User user)
        {
            var userViewModel = new UserViewModel
            {
                idUser = user.idUser,
                Name = user.Name,
                Surname = user.Surname,
                Nickname = user.Nickname,
                Country = user.Country,
                Email = user.Email,
                BirthDate = user.BirthDate,
                RegisterTime = user.RegisterTime
            };
            return userViewModel;
        }
    };
}
