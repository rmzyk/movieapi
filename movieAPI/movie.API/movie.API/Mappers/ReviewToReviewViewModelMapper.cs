﻿using movie.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Mappers
{
    public class ReviewToReviewViewModelMapper
    {
        public static ReviewViewModel ReviewToReviewViewModel(Domain.Review.Review review)
        {
            var reviewViewModel = new ReviewViewModel
            {
                idMovie = review.idMovie,
                idReview = review.idReview,
                idUser = review.idUser,
                Text = review.Text,
                ThumbDown = review.ThumbDown,
                ThumbUp = review.ThumbUp
            };
            return reviewViewModel;  
        }
    }
}
