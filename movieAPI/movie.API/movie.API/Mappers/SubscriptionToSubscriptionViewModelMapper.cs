﻿using movie.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Mappers
{
    public class SubscriptionToSubscriptionViewModelMapper
    {
        public static SubscriptionViewModel SubscriptionToSubscriptionViewModel(Domain.Subscription.Subscription subscription)
        {
            var subscriptionViewModel = new SubscriptionViewModel
            {
                idSubscription = subscription.idSubscription,
                DateActivated = subscription.DateActivated,
                SubscriptionLevel = subscription.SubscriptionLevel,
                ExpireTime = subscription.ExpireTime,
                Type = subscription.Type
            };
            return subscriptionViewModel;
        }
    }
}
