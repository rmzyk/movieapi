﻿using movie.API.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.Mappers
{
    public class GradeToGradeViewModelMapper
    {
        public static GradeViewModel GradeToGradeViewModel(Domain.Grade.Grade grade)
        {
            var gradeViewModel = new GradeViewModel
            {
                idGrade = grade.idGrade,
                idMovie = grade.idMovie,
                idUser = grade.idUser,
                Comment = grade.Comment,
                Rating = grade.Rating
            };
            return gradeViewModel;
        }
    }
}
