﻿using System;

namespace movie.API.ViewModels
{
    public class UserViewModel
    {
        public int idUser { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime RegisterTime { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
    }
}
