﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.ViewModels
{
    public class GradeViewModel
    {
        public int idGrade { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string Comment { get; set; }
        public float Rating { get; set; }
    }
}
