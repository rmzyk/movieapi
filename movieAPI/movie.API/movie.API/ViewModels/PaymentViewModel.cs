﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.ViewModels
{
    public class PaymentViewModel
    {
        public int idPayment { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public string PaymentType { get; set; }
        public DateTime PaymentDate { get; set; }
    }
}
