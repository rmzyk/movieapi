﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.ViewModels
{
    public class SubscriptionViewModel
    {
        public int idSubscription { get; set; }
        public DateTime DateActivated { get; set; }
        public DateTime ExpireTime { get; set; }
        public string Type { get; set; }
        public string SubscriptionLevel { get; set; }
    }
}
