﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.ViewModels
{
    public class MovieViewModel
    {
        public int idMovie { get; set; }
        public string Title { get; set; }
        public DateTime PublicityDate { get; set; }
        public string AgeRestriction { get; set; }
        public float PayPerView { get; set; }
        public string Director { get; set; }
        public int CountPrizes { get; set; }
    }
}
