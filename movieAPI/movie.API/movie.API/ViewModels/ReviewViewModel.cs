﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.API.ViewModels
{
    public class ReviewViewModel
    {
        public int idReview { get; set; }
        public int idUser { get; set; }
        public int idMovie { get; set; }
        public int ThumbUp { get; set; }
        public int ThumbDown { get; set; }
        public string Text { get; set; }
    }
}
