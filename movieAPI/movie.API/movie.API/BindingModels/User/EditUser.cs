﻿using FluentValidation;
using System;
using System.ComponentModel.DataAnnotations;

namespace movie.Data.Sql.BindingModels
{
    public class EditUser
    {
      
        [Display(Name = "Nickname")]
        public string Nickname { get; set; }

       
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

       
        [Display(Name = "BirthDate")]
        public DateTime BirthDate { get; set; }

       
        [Display(Name = "Name")]
        public string Name { get; set; }

       
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }
    }

 public class EditUserValidator : AbstractValidator<EditUser>
    {
        public EditUserValidator()
        {
            RuleFor(x => x.Nickname).NotNull();
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.BirthDate).NotNull();
            RuleFor(x => x.Name).NotNull();
            RuleFor(x => x.Surname).NotNull();
            RuleFor(x => x.Country).NotNull();
        }
        
    }
}
