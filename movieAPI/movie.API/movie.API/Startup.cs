﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using movie.API.Validations;
using movie.Common.Middlewares;
using movie.Data.Pgsql.Migrations;
using movie.Data.Pgsql.Repositories;
using movie.Data.Sql.BindingModels;
using movie.IData.User;
using movie.IServices.Requests;
using movie.IServices.User;
using movie.Services.User;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace movie.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MovieDbContext>(options => options
                .UseNpgsql(Configuration.GetConnectionString("MovieDbContext")));
            services.AddTransient<DbSeed>();
            services.AddScoped<IValidator<EditUser>, EditUserValidator>();
            services.AddScoped<IValidator<IServices.Requests.CreateUser>, CreateUserValidator>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation();
            services.AddApiVersioning(o =>
           {
              o.ReportApiVersions = true;
               o.UseApiBehavior = false;
           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<MovieDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DbSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }
            app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseMvc();
        }
    }
}
