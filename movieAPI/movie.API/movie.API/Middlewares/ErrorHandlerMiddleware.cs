﻿using Microsoft.AspNetCore.Http;
using movie.Domain.DomainExceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace movie.Common.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(Exception ex)
            {
                var exception = ex.GetBaseException();
                exception.GetType();
                await HandleErrorAsync(context, ex);
            }
        }

        private Task HandleErrorAsync(HttpContext context, Exception exception)
        {
            var exceptionResult = GetException(exception);
            var response = new { message = exceptionResult.Item1 };
            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exceptionResult.Item2;

            return context.Response.WriteAsync(payload);
        }

        private(string, int) GetException(Exception exception)
        {
            var key = _exceptionDictionary.Keys.FirstOrDefault(x => x(exception));
            if(key == null)
            {
                //logging
                return ("Internal Server Error", 500);
            }
            return _exceptionDictionary[key](exception);
        }

        private Dictionary<Func<Exception, bool>, Func<Exception, (string, int)>> _exceptionDictionary =
            new Dictionary<Func<Exception, bool>, Func<Exception, (string, int)>> {
               [Exception=>Exception.GetBaseException() is InvalidBirthDateException]
                = exception => (exception.GetBaseException().Message, 400)
                   
                   };
    }

}
