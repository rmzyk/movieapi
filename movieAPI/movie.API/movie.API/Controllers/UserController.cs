﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movie.API.Validations;
using movie.API.ViewModels;
using movie.Data.Pgsql.DAO;
using movie.Data.Pgsql.Migrations;
using movie.Data.Sql.BindingModels;
using System.Data;
using System.Threading.Tasks;

namespace movie.API.Controllers
{
    [ApiVersion("0.1")]
    [Route("api/v{version:apiVersion}/user")]
    public class UserController : Controller
    {
        private readonly MovieDbContext _context;

        public UserController(MovieDbContext context)
        {
            _context = context;
        }

        [Route("{idUser:min(1)}", Name = "GetUserById")]
        [HttpGet]

        public async Task<IActionResult> GetUserById(int idUser)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.idUser == idUser);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    idUser = user.idUser,
                    Email = user.Email,
                    Name = user.Name,
                    Surname = user.Surname,
                    Nickname = user.Nickname,
                    Country = user.Country,
                    RegisterTime = user.RegisterTime,
                    BirthDate = user.BirthDate
                });
            }
            return NotFound();
        }


        [Route("nickname/{nickname}", Name = "GetUserByNickname")]
        [HttpGet]
        public async Task<IActionResult> GetUserByNickname(string nickname)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.Nickname == nickname);

            if (user != null)
            {
                return Ok(new UserViewModel
                {
                    idUser = user.idUser,
                    Email = user.Email,
                    Name = user.Name,
                    Surname = user.Surname,
                    Nickname = user.Nickname,
                    Country = user.Country,
                    RegisterTime = user.RegisterTime,
                    BirthDate = user.BirthDate
                });
            }
            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = new User
            {
                Name = createUser.Name,
                Email = createUser.Email,
                Surname = createUser.Surname,
                Nickname = createUser.Nickname,
                BirthDate = createUser.BirthDate
            };
            await _context.AddAsync(user);
            await _context.SaveChangesAsync();


            return Created(user.idUser.ToString(), new UserViewModel
            {
                idUser = user.idUser,
                Name = user.Name,
                Surname = user.Surname,
                Email = user.Email,
                Nickname = user.Nickname,
                Country = user.Country,
                RegisterTime = user.RegisterTime,
                BirthDate = user.BirthDate
            });
        }


        [Route("edit/{idUser:min(1)}", Name = "EditUser")]
        [ValidateModel]
        [HttpPatch]
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int idUser)
        {
                var user = await _context.User.FirstOrDefaultAsync(x => x.idUser == idUser);
                user.Name = editUser.Name;
                user.Email = editUser.Email;
                user.Surname = editUser.Surname;
                user.Nickname = editUser.Nickname;
                user.BirthDate = editUser.BirthDate;
                await _context.SaveChangesAsync();
                return Ok(new UserViewModel
                {
                    idUser = user.idUser,
                    Email = user.Email,
                    Name = user.Name,
                    Surname = user.Surname,
                    Nickname = user.Nickname,
                    Country = user.Country,
                    RegisterTime = user.RegisterTime,
                    BirthDate = user.BirthDate
                });
        }
        
        [Route("delete/{idUser:min(1)}", Name = "DeleteUser")]
        [ValidateModel]
        [HttpDelete]
        public async Task<IActionResult> DeleteUser(int idUser, bool? saveChangesError=false)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.idUser == idUser);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return NoContent();
        }



    }
}
