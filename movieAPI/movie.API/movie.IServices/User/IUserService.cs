﻿using movie.IServices.Requests;
using System.Threading.Tasks;

namespace movie.IServices.User
{
    public interface IUserService
    {
        Task<Domain.User.User> CreateUser(CreateUser createUser);
    }
}
