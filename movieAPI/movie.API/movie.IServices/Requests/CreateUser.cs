﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.IServices.Requests
{
    public class CreateUser
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string Nickname { get; set; }
        public string Email { get; set; }
    }
}
