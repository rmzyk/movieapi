﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Common.Enums
{
    public enum SubscriptionType
    {
       Both = 2,
       Series = 1,
       Movies = 0
    }
}
