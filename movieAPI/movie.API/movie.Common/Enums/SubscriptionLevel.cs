﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Common.Enums
{
    public enum SubscriptionLevel
    {
        Full = 2,
        Semi = 1,
        None = 0
    }
}
