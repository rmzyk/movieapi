﻿using System;
using System.Collections.Generic;
using System.Text;

namespace movie.Common.Extensions
{
    public static class ListHelpers
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            var random = new Random();

            var i = list.Count;
            while (i > 1)
            {
                i--;
                var j = random.Next(i + 1);
                var value = list[j];
                list[j] = list[i];
                list[i] = value;
            }
        }
    }
}
